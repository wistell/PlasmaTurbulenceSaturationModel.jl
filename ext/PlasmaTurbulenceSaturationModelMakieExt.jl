module PlasmaTurbulenceSaturationModelMakieExt

using PlasmaTurbulenceSaturationModel
Base.isdefined(Base, :get_extension) ? (using Makie) : (using ..Makie)
Base.isdefined(Base, :get_extension) ? (using LaTeXStrings) : (using ..LaTeXStrings)
Base.isdefined(Base, :get_extension) ? (using Printf) : (using ..Printf)

function PTSM.plot_gamma_contours(model::PTSM.FluidModel;
                             resolution=(900,600),
                             fontsize=25,
                             colormap=:inferno,
                             clevels=25,
                  			 title= "Growth rates",
                             font = "new computer modern latin",
                             negative_modes=false,
                             colorrange = nothing,
                            )

    last_kx = negative_modes ? size(model.grid, 1) : div(size(model.grid, 1), 2) + 1
    kx = model.grid.k_perp.kx[1:last_kx, 1]
    ky = model.grid.k_perp.ky[1, :]
    kx2 = div(length(kx), 2)
    unstable_γ = map(I -> imag(model.ω[1, 1, I]), CartesianIndices((PTSM.n_kx_modes(model; neg_kx = negative_modes), PTSM.n_ky_modes(model))))
    if negative_modes
        unstable_γ = circshift(unstable_γ, (kx2, 0))
        kx = circshift(kx, kx2)
    end

    f = Figure(resolution=resolution, font = font)

    fontsize_theme = Theme(fontsize = fontsize, font = font)
    set_theme!(fontsize_theme)
    gl = GridLayout(f[1, 1])
    ax = Axis(gl[1, 1], title = title, titlefont = font,
                xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",
                xlabelsize=fontsize, ylabelsize=fontsize,
                aspect = AxisAspect(abs(last(kx))/last(ky)))
    cof = contourf!(ax, kx, ky, unstable_γ, levels = clevels, colormap=colormap,
    colorrange = isnothing(colorrange) ? (minimum(unstable_γ) >= 0.0 ? 0.0 : minimum(unstable_γ), maximum(unstable_γ)) : colorrange )

    Colorbar(gl[1, 2], cof, label=L"\gamma\, (c_s/a)")
    tightlimits!(ax)
    colgap!(gl, 5)
    f

end

function PTSM.plotModeSpectrum(model::PlasmaTurbulenceSaturationModel.FluidModel;
		resolution=(1600,900),
		fontsize=25,
		title="Mode spectrum",
		)

	kx = PTSM.kx(model,negativeModes=true)
 	ky = PTSM.ky(model)
  kx2 = div(length(kx), 2)
	evals = model.eigenvalues
	unstable_γ = circshift(map(x->imag(x[1]),evals), (kx2,0))
	unstable_ωᵣ = circshift(map(x->-real(x[1]),evals), (kx2,0))

	ωᵣlist = map(x->unstable_ωᵣ[x], 1:length(unstable_ωᵣ))
	γlist = map(x->unstable_γ[x], 1:length(unstable_γ))

	f = Figure()

	fontsize_theme = Theme(fontsize = fontsize)
	set_theme!(fontsize_theme)

	ax = Axis(f[1, 1], title = "ωᵣ vs. γ", xlabel = "γ", ylabel = "ωᵣ")
	scatter!(γlist, ωᵣlist)
	hlines!(ax, [0], color=:black)
	vlines!(ax, [0], color=:black)
	
	f
end

#=
function PTSM.plot_triplet_overlap(model::PlasmaTurbulenceSaturationModel.FluidModel,
                              k_perp1::Union{Tuple{NTuple{2, Int}, Int}, Tuple{CartesianIndex, Int}, Tuple{SpectralPoint, Int}},
                              k_perp2::Union{Tuple{NTuple{2, Int}, Int}, Tuple{CartesianIndex, Int}, Tuple{SpectralPoint, Int}},
                              k_perp3::Union{Tuple{NTuple{2, Int}, Int}, Tuple{CartesianIndex, Int}, Tuple{SpectralPoint, Int}};
                              kwargs...
                             )
    kx1 = k_perp1[1][1]; ky1 = k_perp1[1][2];
    kx2 = k_perp2[1][1]; ky2 = k_perp2[1][2];
    kx3 = k_perp3[1][1]; ky3 = k_perp3[1][2];
    mode_1 = model.spectrum[kx1, ky1].modes[k_perp1[2]]
    mode_2 = model.spectrum[kx2, ky2].modes[k_perp2[2]]
    mode_3 = model.spectrum[kx3, ky3].modes[k_perp3[2]]
    return PTSM.plot_triplet_overlap(mode_1, mode_2, mode_3; kwargs...)
end

function PTSM.plot_triplet_overlap(mode_1::DriftWave{N, T, Fields},
                              mode_2::DriftWave{N, T, Fields},
                              mode_3::DriftWave{N, T, Fields};
                              fields::Union{Symbol, Int, Vector{Symbol}, Vector{Int}} = 1,
                              resolution=(800,500),
                              fontsize=25,
                              title=L"|\phi|",
                              font = "new computer modern roman",
                              linewidth = 4,
                              colorscheme = :Dark2_8,                       
                             ) where {N, T, Fields}

    if !(fields isa Vector)
        fields = fill(fields, 3)
    end
    ϕ_1 = real.(mode_1.β[fields[1]].itp.coefs.parent[2:end-1])
    ϕ_1 ./= maximum(abs.(ϕ_1))
    ϕ_2 = real.(mode_2.β[fields[2]].itp.coefs.parent[2:end-1])
    ϕ_2 ./= maximum(abs.(ϕ_2))
    ϕ_3 = real.(mode_3.β[fields[3]].itp.coefs.parent[2:end-1])
    ϕ_3 ./= maximum(abs.(ϕ_3))
    p1 = range(start = mode_1.η_span.min, stop = mode_1.η_span.max, length = length(ϕ_1))
    p2 = range(start = mode_2.η_span.min, stop = mode_2.η_span.max, length = length(ϕ_2))
    p3 = range(start = mode_3.η_span.min, stop = mode_3.η_span.max, length = length(ϕ_3))
    #label_string_1 = LaTeXString(@sprintf("(k_x=%.3f,k_y=%.3f)", mode_1.k_perp.kx, mode_1.k_perp.ky))
    #label_string_2 = LaTeXString(@sprintf("(k_x=%.3f,k_y=%.3f)", mode_2.k_perp.kx, mode_2.k_perp.ky))
    #label_string_3 = LaTeXString(@sprintf("(k_x=%.3f,k_y=%.3f)", mode_3.k_perp.kx, mode_3.k_perp.ky))

	f = Figure(resolution = resolution, font = font)

	fontsize_theme = Theme(fontsize = fontsize, font = font)
	set_theme!(fontsize_theme)
    x_min = minimum([first(p1), first(p2), first(p3)])
    x_min = (1.0 + (x_min < 0 ? 0.05 : -0.05)) * x_min
    x_max = maximum([last(p1), last(p2), last(p3)])
    x_max = (1.0 + (x_max > 0 ? 0.05 : -0.05)) * x_max
	ax = Axis(f[1,1], title = title, titlesize = fontsize,
              xlabel = "distance along fieldline", xlabelsize = fontsize, ylabelsize = fontsize,
              limits = ((x_min, x_max), (-1.0, 1.0)))

    lines!(ax, [x_min, x_max], [0.0, 0.0],
           color = :black, linewidth = linewidth,)
    lines!(ax, p3, ϕ_3, color = :gray, linewidth = linewidth, 
           label = "Coupling", colorscheme = colorscheme)
    lines!(ax, p2, ϕ_2, color = :blue, linewidth = linewidth,
           label = "Stable", colorscheme = colorscheme)
    lines!(ax, p1, ϕ_1, color = :red, linewidth = linewidth,
           label = "Unstable", colorscheme = colorscheme)
    axislegend(ax; position = :rt)
	f
end
=#
function PTSM.plot_tau_C(model::PlasmaTurbulenceSaturationModel.FluidModel{N, T};
                    resolution=(700,600),
                    fontsize=25,
                    colormap=:vik,
                    clevels=25,
                    cbarlims=nothing,
                    label=L"|\mathrm{Re}(\tau C)|",
                    zonal_couplings::Bool=true,
                    scale=identity
                    ) where {N, T}
    kx = PlasmaTurbulenceSaturationModel.kx(model; negative_modes=false)
    ky = PlasmaTurbulenceSaturationModel.ky(model)

    f = Figure(resolution=resolution, font = "CMU Serif")
    
    #This will use max(|Re(τC)|)
    abs_real_tau_C = Array{T}(undef,(length(kx),length(ky)))
    tau_times_C = Array{Complex{T}}(undef,(N, n_ev(model) ^ 3, PTSM.n_kx_modes(model; neg_kx = false), PTSM.n_ky_modes(model)))
    for j in eachindex(ky)
        for i in eachindex(kx)
            tau_kx_ky = view(model.τ, :, :, i, j, :, :)
            C_kx_ky = view(model.CC, :, :, i, j, :, :)
            for k in eachindex(tau_C_kx_ky)
                tau_times_C[k] = tau_kx_ky[k]*C_kx_ky[k]
            end
            abs_real_tau_C[i,j] = zonal_couplings ? maximum(abs.(real.(tau_times_C[1,:,:,j]))) : maximum(abs.(real.(cat(tau_times_C[3,:,:,1:j-1],tau_times_C[3,:,:,j+1:end],dims=3))))
        end
    end
    title = zonal_couplings ? "Zonal" : "Nonzonal"
    Axis(f[1, 1], xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",xlabelsize=fontsize, ylabelsize=fontsize, title=title)
    
    if isnothing(cbarlims)
        co = heatmap!(kx, ky, abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    else
        co = heatmap!(kx, ky, abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale,colorrange=cbarlims)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    end
    println(minimum(abs_real_tau_C))
    println(maximum(abs_real_tau_C))
   
    f
    
end

function PTSM.plot_gamma_over_tau_C(model::PlasmaTurbulenceSaturationModel.FluidModel{N, T};
                    resolution=(700,600),
                    fontsize=25,
                    colormap=:vik,
                    clevels=25,
                    cbarlims=nothing,
                    label=L"\gamma/|\sum_{k_x,k_y}\mathrm{Re}(\tau C)|",
                    scale=identity,
                    negative_modes=false
                    ) where {N, T}
    last_kx = negative_modes ? size(model.grid, 1) : div(size(model.grid, 1), 2) + 1
    kx = PTSM.kx(model; negative_modes = negative_modes)
    ky = PTSM.ky(model)
    kx2 = div(length(kx), 2)
    unstable_γ = map(I -> imag(model.ω[1, 1, I]), CartesianIndices((PTSM.n_kx_modes(model; neg_kx = negative_modes), PTSM.n_ky_modes(model))))
    if negative_modes
        unstable_γ = circshift(unstable_γ, (kx2, 0))
        kx = circshift(kx, kx2)
    end

    f = Figure(resolution=resolution, font = "CMU Serif")
    
    #This will use max(|Re(τC)|)
    abs_real_tau_C = Array{T}(undef,(length(kx),length(ky)))
    tau_times_C = Array{Complex{T}}(undef,(N, n_ev(model) ^ 3, PTSM.n_kx_modes(model; neg_kx = false), PTSM.n_ky_modes(model)))
    for j in eachindex(ky)
        for i in eachindex(kx)
            tau_kx_ky = view(model.τ, :, :, i, j, :, :)
            C_kx_ky = view(model.CC, :, :, i, j, :, :)
            for k in eachindex(tau_C_kx_ky)
                tau_times_C[k] = tau_kx_ky[k]*C_kx_ky[k]
            end
            abs_real_tau_C[i,j] = sum(abs.(real.(tau_times_C[1,1,:,j])))+sum(abs.(real.(cat(tau_times_C[3,:,:,1:j-1],tau_times_C[3,:,:,j+1:end],dims=3))))
        end
    end
    Axis(f[1, 1], xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",xlabelsize=fontsize, ylabelsize=fontsize)
    
    if isnothing(cbarlims)
        co = heatmap!(kx, ky, unstable_γ./abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    else
        co = heatmap!(kx, ky, unstable_γ./abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale,colorrange=cbarlims)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    end
    println(sum(unstable_γ./abs_real_tau_C))
    f
    
end

function PTSM.plotMarginalContours(model::PlasmaTurbulenceSaturationModel.FluidModel;
                           resolution=(900,600),
                           fontsize=25,
                           colormap=:vik,
                           clevels=25,
                  			   title= "Marginal mode frequencies",
                           negativeModes=false,
                          )
  kx = PlasmaTurbulenceSaturationModel.kx(model,negativeModes=true)
  ky = PlasmaTurbulenceSaturationModel.ky(model)
  kx2 = div(length(kx),2)
  startKx = negativeModes ? 1 : kx2+1
  evals = model.eigenvalues
  marginal_ω = circshift(map(x->-real(x[3]),evals), (kx2,0))


  f = Figure(resolution=resolution)

  fontsize_theme = Theme(fontsize = fontsize)
  set_theme!(fontsize_theme)

  Axis(f[1, 1], title = title, xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s", xlabelsize=fontsize, ylabelsize=fontsize)
  co = contourf!(kx[startKx:end], ky, marginal_ω[startKx:end,:], levels = clevels, colormap=colormap)

  Colorbar(f[1, 2], co, label=L"\gamma (c_s/a)")

  f

end

end

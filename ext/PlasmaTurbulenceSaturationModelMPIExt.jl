module PlasmaTurbulenceSaturationModelMPIExt

using PlasmaTurbulenceSaturationModel
isdefined(Base, :get_extension) ? (using MPI) : (using ..MPI)

function PTSM.compute_linear_spectrum!(model::PTSM.FluidModel{3, T},
                                       comm::MPI.Comm;
                                      ) where {T}
    rank = MPI.Comm_rank(comm)
    comm_size = MPI.Comm_size(comm)
    n_kx = PTSM.n_kx_modes(model, neg_kx = false)
    n_ky = PTSM.n_ky_modes(model)
    ω = Matrix{Complex{T}}(undef, 3, PTSM.n_ev(model))
    β = Matrix{Complex{T}}(undef, model.problem.n_points, PTSM.n_ev(model.problem))

    if rank == 0
        
        initial_kx_ky = PTSM.initial_guess(model)
        initial_ky_index = initial_kx_ky[2]
        start_ky_index = initial_ky_index
        start_kx_index = initial_kx_ky[1]

        init = true
        for ky_index in start_ky_index:-1:1
            PTSM.compute_linear_mode!(model, CartesianIndex(start_kx_index, ky_index), init)
            if init
                init = false
            end
        end
        for ky_index in start_ky_index+1:n_ky
            PTSM.compute_linear_mode!(model, CartesianIndex(start_kx_index, ky_index))
        end

        for ky_index in 1:n_ky
            MPI.bcast(ky_index, comm; root = 0)
            for m in axes(ω, 2)
                ω[:, m] .= model.ω[:, m, 1, ky_index]
                β[:, m] .= model.β[:, m, 1, ky_index]
            end
            MPI.Bcast!(ω, comm; root = 0)
            MPI.Bcast!(β, comm; root = 0)
        end

        rank_status = fill(true, comm_size - 1)
        indices = CartesianIndices((1, 1:n_ky))
        I, S = nothing, nothing
        for i in eachindex(rank_status)
            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end
            MPI.send(I, comm; dest = i, tag = i)
            if I == CartesianIndex(-1, -1)
                rank_status[i] = false 
            end
        end

        finished = all(r -> !r, rank_status) 
        while !finished
            r = MPI.recv(comm; source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG)
            J = MPI.recv(comm; source = r, tag = 2 * comm_size + r)
        
            for k in 2:n_kx
                K = CartesianIndex(k, J[2])
                index_range = model.index_range[K]

                MPI.Recv!(ω, comm; source = r, tag = 3 * comm_size + r)
                MPI.Recv!(β, comm; source = r, tag = 4 * comm_size + r)

                copyto!(model.problem.eig_vals, map(i -> ω[1, i], 1:PTSM.n_ev(model.problem)))
                copyto!(model.problem.eig_vecs, β)
                copyto!(model.problem.cache.g.xx, model.geometry.g.xx[index_range])
                copyto!(model.problem.cache.g.xy, model.geometry.g.xy[index_range])
                copyto!(model.problem.cache.g.yy, model.geometry.g.yy[index_range])
                copyto!(model.problem.cache.B, model.geometry.B[index_range])
                copyto!(model.problem.cache.jac, model.geometry.jac[index_range])
                PTSM.damping_drive_term!(model.problem.cache.Bk, model.problem.cache.Dk, index_range, zero(T), model.grid.k_perp[K].ky, model.geometry)
            
                PTSM.update_spectrum!(model, K)


            end


            try
                I, S = !isnothing(S) ? iterate(indices, S) : iterate(indices)
            catch
                I, S = CartesianIndex(-1, -1), S
            end

            MPI.send(I, comm; dest = r, tag = r)
            if I == CartesianIndex(-1, -1)
                rank_status[r] = false 
            end
            if all(i -> !i, rank_status)
                finished = true
            end
        end
        GC.gc()
    else
        I = CartesianIndex(0, 0)
        for _ in 1:n_ky
            ky_index = MPI.bcast(nothing, comm; root = 0)
            MPI.Bcast!(ω, comm; root = 0)
            MPI.Bcast!(β, comm; root = 0)
            for m in axes(ω, 2)
                model.ω[:, m, 1, ky_index] .= ω[:, m]
                model.β[:, m, 1, ky_index] .= β[:, m]
            end
        end
        
        finished = false
    
        while !finished


            I = MPI.recv(comm; source = 0, tag = rank)

#@debug "Rank $rank received $I from head with tag $rank"

            if I != CartesianIndex(-1, -1)
                for j in 2:n_kx
                    J = CartesianIndex(j, I[2])
                    #init = J[1] == 1 ? true : false

#@debug "Rank $rank is computing mode $J with init = $init"

                    PTSM.compute_linear_mode!(model, J)

@debug "Rank $rank has computed mode $J with frequencies $(model.ω[:, 1, J].ω)"

                end

@debug "Rank $rank finished, sending $rank to head with tag $(comm_size + rank)"

                MPI.send(rank, comm; dest = 0, tag = comm_size + rank)

#@debug "Rank $rank sending $I to head with tag $(2 * comm_size + rank)"

                MPI.send(I, comm; dest = 0, tag = 2 * comm_size + rank)

                for j in 2:n_kx 
                    J = CartesianIndex(j, I[2])
                    for m in axes(ω, 2)
                        @debug "Rank $rank ready to send $(model.ω[:, m, J])"
                        ω[:, m] .= model.ω[:, m, J]
                        β[:, m] .= model.β[:, m, J]
                    end
                    @debug "Rank $rank sending $ω to head with tag $(2 * comm_size + rank)"
                    MPI.Send(ω, comm; dest = 0, tag = 3 * comm_size + rank)
                    MPI.Send(β, comm; dest = 0, tag = 4 * comm_size + rank)
                end
            else
                finished = true
            end
        end
        GC.gc()
    end
end

function PTSM.quasilinear_flux(eq_surface,
                               comm::MPI.Comm;
                               kwargs...
                              )
    rank = MPI.Comm_rank(comm)
    PTSM.setup_linear_model(eq_surface; kwargs...)
    PTSM.compute_linear_spectrum!(model, comm)
    if rank == 0
        return PTSM.quasilinear_flux(model)
    else
        return nothing
    end
    GC.gc()
end

end

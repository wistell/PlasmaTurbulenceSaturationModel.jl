module PlasmaTurbulenceSaturationModelVMECExt

using PlasmaTurbulenceSaturationModel
isdefined(Base, :get_extension) ? (using PlasmaEquilibriumToolkit) : (using ..PlasmaEquilibriumToolkit)
isdefined(Base, :get_extension) ? (using VMEC) : (using ..VMEC)
isdefined(Base, :get_extension) ? (using GeneTools) : (using ..GeneTools)

function PTSM.setup_linear_model(surface::VmecSurface;
                                 n_fields=3,
                                 n_ev=1,
                                 prec=Float64,
                                 Δkx = 0.1,
                                 Δky = 0.1,
                                 kxSpecLimit = 0.2,
                                 kySpecLimit = 0.6,
                                 kxSpecPower = 2.0,
                                 kySpecPower = 4/3,
                                 ∇T = 3.0,
                                 ∇n = 0.0,
                                 τ = 1.0,
                                 β = 1.0,
                                 solver = PTSM.ArnoldiMethod(),
                                 soln_interval=8π,
                                 ε=400.0,
                                 errorOrder=4,
                                 hyperdiffusionOrder=2,
                                 n_points = 1024,
                                )
    iota = surface.iota[1];
    s_hat = VMEC.shat(surface);
    nfp = surface.nfp;

    plasma = PTSM.PlasmaParameters(∇T, ∇n, τ, β, FloatType = prec)
    
    problem = PTSM.LinearProblem(solver=solver, n_ev = n_ev, n_points = n_points,
                                 soln_interval=soln_interval, ε=ε,error_order=errorOrder,
                                 hyper_diffusion_order=hyperdiffusionOrder,
                                 FloatType = prec)

    δkx = convert(prec, Δkx)
    δky = convert(prec, Δky)
    nkx_pos =floor(Int, one(prec) / δkx) + 1
    
    max_θ = δkx * nkx_pos/(abs(s_hat) * δky) + soln_interval
    n_fieldline_points = convert(Int, ceil(2 * max_θ * n_points / soln_interval))

    # Defined in θ
    θ_range = range(start = -max_θ, stop = max_θ, length = n_fieldline_points)
    #vector2range(θ_range)

    p = MagneticCoordinateCurve(PestCoordinates, surface.s * surface.phi[end] / (2π) * surface.signgs, 0.0, -θ_range/iota[1])

    metric, modB, sqrtg, K1, K2, ∂B∂θ = GeneTools.gene_geometry_coefficients(GeneFromPest(), p, surface)
    #θ = map(i->-i.ζ*iota,p)
    geom = PTSM.FieldlineGeometry(metric, modB, sqrtg, K1, K2, ∂B∂θ, θ_range; ι = iota, s_hat = s_hat, nfp = nfp, FloatType = prec)
    grid = PTSM.SpectralGrid(geom, problem; Δkx=Δkx, Δky=Δky,
                             kxSpecLimit=kxSpecLimit, kySpecLimit=kySpecLimit,
                             kxSpecPower=kxSpecPower, kySpecPower=kySpecPower)

    return PTSM.FluidModel(plasma, grid, problem, geom; n_fields = n_fields, n_ev = n_ev, FloatType=prec)

end

end

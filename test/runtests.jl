using PlasmaTurbulenceSaturationModel
using Test

@testset "PlasmaTurbulenceSaturationModel.jl" begin
    include("vmec_tests.jl")
end

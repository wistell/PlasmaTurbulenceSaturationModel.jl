
function setup_sparse_operators(::Val{3},
                                T::Type,
                                n_points::Integer,
                                error_order::Integer,
                                hyper_diffusion_order::Integer,
                                fdm_stencils::Matrix{S},
                                hyp_diff_stencils::Matrix{S};
                               ) where {S}

    lhs_output = zeros(T, 3 * n_points, 3 * n_points)

    for index in (div(error_order, 2)+1):(n_points-div(error_order, 2))
        j = div(error_order, 2) + 1
        jr = range(start = 1, stop = error_order + 1)
        
        lhs_output[index, index .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
        lhs_output[index, (n_points + index) .+ fdm_stencils[jr, j]] .= one(Complex{T}) 
        lhs_output[index, 2 * n_points + index] = one(Complex{T})

        lhs_output[n_points + index, index .+ fdm_stencils[jr, j]] .= one(Complex{T})
        lhs_output[n_points + index, (n_points + index) .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
        lhs_output[n_points + index, (2 * n_points + index) .+ fdm_stencils[jr, j]] .= one(Complex{T}) 
            
        lhs_output[2 * n_points + index, index] = one(Complex{T})
        lhs_output[2 * n_points + index, (2 * n_points + index) .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
    end 

    for index in 1:div(error_order, 2)
        j = index
        jr = range(start = 2, stop = error_order + 1)

        lhs_output[index, index .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
        lhs_output[index, (n_points + index) .+ fdm_stencils[jr, j]] .= one(Complex{T}) 
        lhs_output[index, (2 * n_points + index)] = one(Complex{T})

        lhs_output[n_points + index, index .+ fdm_stencils[jr, j]] .= one(Complex{T})
        lhs_output[n_points + index, (n_points + index) .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
        lhs_output[n_points + index, (2 * n_points + index) .+ fdm_stencils[jr, j]] .= one(Complex{T})
            
        lhs_output[2 * n_points + index, index] = one(Complex{T})
        lhs_output[2 * n_points + index, (2 * n_points + index) .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})

        j = error_order + 2 - index
        jr = range(start = 1, stop = error_order)
        
        lhs_output[n_points - index + 1, (n_points - index + 1) .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
        lhs_output[n_points - index + 1, (2 * n_points - index + 1) .+ fdm_stencils[jr, j]] .= one(Complex{T}) 
        lhs_output[n_points - index + 1, (3 * n_points - index + 1)] = one(Complex{T})

        lhs_output[2 * n_points - index + 1, (n_points - index + 1) .+ fdm_stencils[jr, j]] .= one(Complex{T})
        lhs_output[2 * n_points - index + 1, (2 * n_points - index + 1) .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
        lhs_output[2 * n_points - index + 1, (3 * n_points - index + 1) .+ fdm_stencils[jr, j]] .= one(Complex{T}) 
            
        lhs_output[3 * n_points - index + 1, n_points - index + 1] = one(Complex{T})
        lhs_output[3 * n_points - index + 1, (3 * n_points - index + 1) .+ hyp_diff_stencils[jr, j]] .= one(Complex{T})
    end

    rhs_output = zeros(T, 3 * n_points, 3 * n_points)

    for index in 1:n_points
        rhs_output[index, index] = one(T)
        rhs_output[index, 2 * n_points + index] = one(T)
        rhs_output[n_points + index, n_points + index] = one(T)
        rhs_output[2 * n_points + index, 2 * n_points + index] = one(T)
    end

    lhs = Complex{T}.(sparse(lhs_output))
    rhs = Complex{T}.(sparse(rhs_output))
    lhs_nrows = (circshift(lhs.colptr, -1) .- lhs.colptr)[1:end-1]
    buffer = Vector{Complex{T}}(undef, maximum(lhs_nrows))
    op = rhs * lhs

    return lhs, rhs, lhs_nrows, op, buffer
end


"""
    build_linear_operators!(problem, which_mode, geom, plasma)

Build the linear operator
"""
function build_linear_operators!(problem::LinearProblem{3, S, T, E},
                                 k_perp::NamedTuple,
                                 index_range::UnitRange,
                                 geom::FieldlineGeometry{T},
                                 plasma::PlasmaParameters;
                                ) where {S <: ArnoldiMethod, T, E}
    kx = k_perp.kx
    ky = k_perp.ky
    
    copyto!(problem.cache.B, geom.B[index_range])
    copyto!(problem.cache.jac, geom.jac[index_range])
    damping_drive_term!(problem.cache.Bk, problem.cache.Dk, index_range, kx, ky, geom)

    # For convenience extract the relevant plasma parameters
    ∇n = plasma.∇n
    ∇T = plasma.∇T
    τ = plasma.τ

    # Set the numerical hyperdiffusion parameters
    Δθ = step(geom.z_range)
    
    hyper_diff_ε = problem.ε
    hyp_z_coeff = hyper_diff_ε*(im*0.5*Δθ)^problem.hyper_diffusion_order

    copyto!(problem.fdm_prefactor, (-im/Δθ) ./ problem.cache.jac ./ problem.cache.B)
    copyto!(problem.hyp_diff_prefactor, (-im *hyp_z_coeff) ./ (Δθ .* problem.cache.jac .* problem.cache.B) .^ problem.hyper_diffusion_order)

    n_points = problem.n_points
    lhs_nonzero = problem.lhs.nzval
    rhs_nonzero = problem.rhs.nzval
    lhs_nrows = problem.lhs_nrows
    buffer = problem.buffer
    lhs_colptr = problem.lhs.colptr
    rhs_colptr = problem.rhs.colptr

    @inbounds for index in 1:n_points
        ρ = 1 + 5 * τ/3
        σ = (1.0 /(1.0 + problem.cache.Bk[index] * ρ))
        ν = -(problem.cache.Bk[index] * τ /(1.0 + problem.cache.Bk[index] * ρ))
        ξ = (ky * ∇n/problem.cache.B[index] + problem.cache.Dk[index] * ρ)
        χ = ky * (∇T - 2 * ∇n / 3) / problem.cache.B[index]
        λ = problem.cache.Dk[index] * τ

        col = index
        phi_terms!(buffer, index, lhs_nrows[col], ρ, ξ, χ, n_points,
                   problem.fdm_coeffs, problem.fdm_prefactor, problem.hyp_diff_coeffs,
                   problem.hyp_diff_prefactor, problem.boundary_coeff_inds)
        lhs_nonzero[lhs_colptr[col]:lhs_colptr[col+1]-1] .= buffer[1:lhs_nrows[col]]
        
        rhs_nonzero[rhs_colptr[col]:rhs_colptr[col+1]-1] .= [σ] 

        col = n_points + index
        v_par_terms!(buffer, index, lhs_nrows[col], n_points,
                     problem.fdm_coeffs, problem.fdm_prefactor, problem.hyp_diff_coeffs,
                     problem.hyp_diff_prefactor, problem.boundary_coeff_inds)
        lhs_nonzero[lhs_colptr[col]:lhs_colptr[col+1]-1] .= buffer[1:lhs_nrows[col]]

        col = 2 * n_points + index
        T_terms!(buffer, index, lhs_nrows[col], λ, τ, n_points,
                 problem.fdm_coeffs, problem.fdm_prefactor, problem.hyp_diff_coeffs,
                 problem.hyp_diff_prefactor, problem.boundary_coeff_inds)
        lhs_nonzero[lhs_colptr[col]:lhs_colptr[col+1]-1] .= buffer[1:lhs_nrows[col]]

        rhs_nonzero[rhs_colptr[col]:rhs_colptr[col+1]-1] .= [ν, one(Complex{T})]
    end

    problem.operator .= problem.rhs * problem.lhs
    return nothing
end

function phi_terms!(buffer::Vector{Complex{T}},
                    col_index::Integer,
                    colptr::Integer,
                    ρ::T,
                    ξ::T,
                    χ::T,
                    n_points::Integer,
                    fdm_coeffs::Matrix{Complex{T}},
                    fdm_scale::Vector{Complex{T}},
                    hyp_diff_coeffs::Matrix{Complex{T}},
                    hyp_diff_scale::Vector{Complex{T}},
                    inds::Vector{Vector{CartesianIndex{2}}};
                   ) where {T}
    n_fd_coeffs = size(fdm_coeffs, 1)
    n_bd_rows = div(n_fd_coeffs, 2) + 1
    n_fill_rows = div(colptr - 1, 2)
    buffer[colptr] = χ
    if n_fd_coeffs <= col_index <= n_points - n_fd_coeffs + 1
        index = n_fd_coeffs
        buffer[1:n_fd_coeffs] .= hyp_diff_scale[col_index:col_index+n_fd_coeffs-1] .* hyp_diff_coeffs[inds[index]]
        buffer[n_bd_rows] += ξ
        buffer[n_fd_coeffs+1:2*n_fd_coeffs] .= ρ .* fdm_scale[col_index:col_index+n_fd_coeffs-1] .* fdm_coeffs[inds[index]]
    elseif col_index < n_fd_coeffs
        index = col_index
        buffer[1:n_fill_rows] .= hyp_diff_scale[col_index:col_index+n_fill_rows-1] .* hyp_diff_coeffs[inds[index]]
        buffer[col_index] += ξ
        buffer[n_fill_rows+1:2*n_fill_rows] .= ρ .* fdm_scale[col_index:col_index+n_fill_rows-1] .* fdm_coeffs[inds[index]]
    elseif col_index > n_points - n_fd_coeffs + 1
        index = col_index - n_points + 2 * n_fd_coeffs - 1
        buffer[1:n_fill_rows] .= hyp_diff_scale[n_points-n_fill_rows+1:n_points] .* hyp_diff_coeffs[inds[index]]
        buffer[n_bd_rows] += ξ
        buffer[n_fill_rows+1:2*n_fill_rows] .= ρ .* fdm_scale[n_points-n_fill_rows+1:n_points] .* fdm_coeffs[inds[index]]
    end
    return nothing
end

function v_par_terms!(buffer::Vector{Complex{T}},
                      col_index::Integer,
                      colptr::Integer,
                      n_points::Integer,
                      fdm_coeffs::Matrix{Complex{T}},
                      fdm_scale::Vector{Complex{T}},
                      hyp_diff_coeffs::Matrix{Complex{T}},
                      hyp_diff_scale::Vector{Complex{T}},
                      inds::Vector{Vector{CartesianIndex{2}}};
                     ) where {T}
    n_fd_coeffs = size(fdm_coeffs, 1)
    n_fill_rows = div(colptr, 2)
    if n_fd_coeffs <= col_index <= n_points - n_fd_coeffs + 1
        index = n_fd_coeffs
        buffer[1:n_fd_coeffs] .= fdm_scale[col_index:col_index+n_fd_coeffs-1] .* fdm_coeffs[inds[index]]
        buffer[n_fd_coeffs+1:2*n_fd_coeffs] .= hyp_diff_scale[col_index:col_index+n_fd_coeffs-1] .* hyp_diff_coeffs[inds[index]]
    elseif col_index < n_fd_coeffs
        index = col_index
        buffer[1:n_fill_rows] .= fdm_scale[col_index:col_index+n_fill_rows-1] .* fdm_coeffs[inds[index]]
        buffer[n_fill_rows+1:2*n_fill_rows] .= hyp_diff_scale[col_index:col_index+n_fill_rows-1] .* hyp_diff_coeffs[inds[index]]
    elseif col_index > n_points - n_fd_coeffs + 1
        index = col_index - n_points + 2 * n_fd_coeffs - 1
        buffer[1:n_fill_rows] .= fdm_scale[n_points-n_fill_rows+1:n_points] .* fdm_coeffs[inds[index]]
        buffer[n_fill_rows+1:2*n_fill_rows] .= hyp_diff_scale[n_points-n_fill_rows+1:n_points] .* hyp_diff_coeffs[inds[index]]
    end
end

function T_terms!(buffer::Vector{Complex{T}},
                  col_index::Integer,
                  colptr::Integer,
                  λ::T,
                  τ::T,
                  n_points::Integer,
                  fdm_coeffs::Matrix{Complex{T}},
                  fdm_scale::Vector{Complex{T}},
                  hyp_diff_coeffs::Matrix{Complex{T}},
                  hyp_diff_scale::Vector{Complex{T}},
                  inds::Vector{Vector{CartesianIndex{2}}};
                 ) where {T}
    n_fd_coeffs = size(fdm_coeffs, 1)
    n_fill_rows = div(colptr - 1, 2)
    buffer[1] = λ
    if n_fd_coeffs <= col_index <= n_points - n_fd_coeffs + 1
        index = n_fd_coeffs
        buffer[2:n_fd_coeffs+1] .= τ .* fdm_scale[col_index:col_index+n_fd_coeffs-1] .* fdm_coeffs[inds[index]]
        buffer[n_fd_coeffs+2:2*n_fd_coeffs+1] .= hyp_diff_scale[col_index:col_index+n_fd_coeffs-1] .* hyp_diff_coeffs[inds[index]]
    elseif col_index < n_fd_coeffs
        index = col_index
        buffer[2:n_fill_rows+1] .= τ .* fdm_scale[col_index:col_index+n_fill_rows-1] .* fdm_coeffs[inds[index]]
        buffer[n_fill_rows+2:2*n_fill_rows+1] .= hyp_diff_scale[col_index:col_index+n_fill_rows-1] .* hyp_diff_coeffs[inds[index]]
    elseif col_index > n_points - n_fd_coeffs + 1
        index = col_index - n_points + 2 * n_fd_coeffs - 1
        buffer[2:n_fill_rows+1] .= τ .* fdm_scale[n_points-n_fill_rows+1:n_points] .* fdm_coeffs[inds[index]]
        buffer[n_fill_rows+2:2*n_fill_rows+1] .= hyp_diff_scale[n_points-n_fill_rows+1:n_points] .* hyp_diff_coeffs[inds[index]]
    end
    return nothing
end

## DEPRECATED ##
#=
function dense_linear_operators(dw::Union{DriftWave{3, T, Fields}, DriftWaveBundle{3, T, E, Fields}},
                                geom::FieldlineGeometry{T},
                                problem::LinearProblem{3, S, T, E},
                                plasma::PlasmaParameters;
                               ) where {S, T, E, Fields}
  kx = dw.k_perp.kx
  ky = dw.k_perp.ky
  
  # For convenience extract the relevant plasma parameters
  ∇n = plasma.∇n
  ∇T = plasma.∇T
  τ = plasma.τ

  # Set the numerical hyperdiffusion parameters
  θ_range = range(start = dw.η_span.min, stop = dw.η_span.max, length = problem.n_points)
  Δθ = step(θ_range)
  n_points = problem.n_points
  rank = 3 * n_points
  lhs = zeros(Complex{T}, (rank, rank))
  rhs = Matrix{Complex{T}}(I, (rank, rank))

  hyper_diff_ε = problem.ε
  hyp_z_coeff = hyper_diff_ε*(im*0.5*Δθ)^problem.hyper_diffusion_order

  @inbounds for (index, θ) in enumerate(θ_range)
    Bk_θ, Dk_θ = damping_drive_term(θ, kx, ky, geom)
    B = geom.B(θ)
    jac = geom.jac(θ)

    # Compute the finite difference splines for the first order derivative operator
    # e.g. ∂/∂z → [c₁,c₂,…,cₙ₋₁,cₙ] where n = model.grid.derivative_error_order + 1
    fdm = parallel_derivative(index,n_points, 1, problem.derivative_error_order)
    # Compute the finite difference spline for the n-th order derivative operator
    # where n is specified by model.grid.hyper_diffusion_order
    hyper_diff = parallel_derivative(index, n_points,
                                     problem.hyper_diffusion_order,
                                     problem.derivative_error_order)

    # Deal with boundaries where the point beyond the range is assumed to be zero
    deriv_indices = findall(x -> ((x + index) > 0 && (x + index) <= n_points), fdm.grid)
    hyper_diff_indices = findall(x -> ((x + index) > 0 && (x + index) <= n_points), hyper_diff.grid)

    fdm_grid = fdm.grid[deriv_indices]
    # Apply the prefactor -i/√gB to each derivative term
    fdm_coefs = convert.(Complex{T}, fdm.coefs[deriv_indices])*(-im/(Δθ*jac*B))

    hyper_diff_grid = hyper_diff.grid[hyper_diff_indices]
    # Approximate the n-th order hyperdiffusion operator by taking only the
    # leading order term
    hyper_diff_coefs = hyper_diff.coefs[hyper_diff_indices] *
                       (-im * hyp_z_coeff/(Δθ * jac * B)^problem.hyper_diffusion_order)

    # Use array views to efficiently access the underlying data to apply the
    # finite differnce splines. For the three-field model with operator rank
    # 3N × 3N, the system can be broken into N × N blocks:
    #     || Φₖ ||    || A1 | A2 | A3 ||   || Φₖ ||
    # A ⋅ || Uₖ || ⇒  || A4 | A5 | A6 || ⋅ || Uₖ ||
    #     || Tₖ ||    || A7 | A8 | A9 ||   || Tₖ ||

    # Left-hand side operator
    # Block 1
    lhsView = view(lhs, index, index .+ hyper_diff_grid)
    lhsView .+= hyper_diff_coefs
    lhs[index, index] += ky * ∇n/B + Dk_θ * (1 + 5 * τ/3)

    # Block 2
    lhsView = view(lhs, index, (n_points+index) .+ fdm_grid)
    lhsView .+= fdm_coefs

    # Block 3
    lhs[index, 2 * n_points+index] += Dk_θ * τ

    # Block 4
    lhsView = view(lhs,n_points+index,index .+ fdm_grid)
    lhsView .+= fdm_coefs * (1 + 5 * τ/3)

    # Block 5
    lhsView = view(lhs, n_points + index, (n_points + index) .+ hyper_diff_grid)
    lhsView .+= hyper_diff_coefs

    # Block 6
    lhsView = view(lhs, n_points + index,(2 * n_points + index) .+ fdm_grid)
    lhsView .+= fdm_coefs * τ

    # Block 7
    lhs[2 * n_points + index, index] += ky * (∇T - 2 * ∇n/3)/B #+ 10*τ*Dk_θ/9
    #lhs[2*nPoints+index,2*nPoints+index] += 5*τ*Dk_θ/3

    # Block 8
    #
    # Block 9
    lhsView = view(lhs, 2 * n_points + index, (2 * n_points + index) .+ hyper_diff_grid)
    lhsView .+= hyper_diff_coefs

    # Right hand side operator
    # Block 1
    rhs[index, index] += Bk_θ * (1 + 5 * τ/3)

    #Block 3
    rhs[index, 2 * n_points + index] += Bk_θ * τ

  end
  return sparse(lhs), sparse(rhs)
end
=#

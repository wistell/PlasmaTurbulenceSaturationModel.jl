
function construct_precond_vector!(::Val{3},
                                   precond_vec::AbstractArray,
                                   irreducible_vec::AbstractArray,
                                   k_perp::NamedTuple,
                                   index_range::UnitRange,
                                   ω::AbstractArray,
                                   plasma::PlasmaParameters,
                                   geom::FieldlineGeometry,
                                   prob::LinearProblem, 
                                  )
    n_points = prob.n_points
    ky = k_perp.ky
    jac_view = view(prob.cache.jac, :)
    B_view = view(prob.cache.B, :)
    jac_view[:] .= geom.jac[index_range]
    B_view[:] .= geom.B[index_range]
    n_points = length(irreducible_vec)
    copyto!(precond_vec, 1, irreducible_vec, 1, n_points)
    copyto!(precond_vec, 2 * n_points + 1, (im * ky * plasma.∇T / ω[1]) .* (irreducible_vec ./ B_view), 1, n_points)
    copyto!(precond_vec, n_points + 1, (-im / ω[1]) .* (precond_vec[1:n_points] .+ precond_vec[2 * n_points + 1 : end]), 1, n_points)
    parallel_derivative!(prob.cache.∂_vec, precond_vec[n_points + 1 : 2 * n_points], prob)
    copyto!(precond_vec, n_points + 1, prob.cache.∂_vec, 1, n_points)
    return nothing
end


"""
compute_linear_roots(k_perp::NamedTupe,
                     vec::Union{SplineType, Function},
                     ∂_vec::Union{SplineType, Function},
                     plasma::PlasmaParameters,
                     problem::LinearProblem,
                    ) where {FG <: AbstractFieldlineGeometry}

For a given eigenfunction specfied by `vec` and geometry interval, compute the unstable,
stable and marginally stable solutions, this can provide a check on the direct
eigenvalue calculation.
"""
function compute_linear_roots(k_perp::NamedTuple,
                              vec::AbstractVector,
                              ∂_vec::AbstractVector,
                              plasma::PlasmaParameters{T},
                              problem::LinearProblem{3, S, T, E},
                             ) where {S <: AbstractSolverMethod, T, E}
    ky = k_perp.ky

    L3 = L3_integral(vec, problem.cache.B, problem.cache.jac, problem.cache.Bk, plasma)

    L2 = L2_integral(ky, vec, problem.cache.B, problem.cache.jac, problem.cache.Bk, problem.cache.Dk, plasma)

    L1 = L1_integral(ky, vec, ∂_vec, problem.cache.B, problem.cache.jac, problem.cache.Dk, plasma)

    L0 = L0_integral(ky, ∂_vec, problem.cache.B, problem.cache.jac, plasma)

    # Use the Polynomials package to find the roots of the cubic equation
    # ω³ + (L₂/L₃)ω² + (L₁/L₃)ω + L₀/L₃ = 0
    roots = Polynomials.roots(Polynomial([L0/L3,L1/L3,L2/L3,1.0]))

    # Sort the modes, it may be more efficient to move this outside the function
    unstableMode = findfirst(x->imag(x)>0, roots)
    stableMode = findfirst(x->imag(x)<0, roots)
    marginal_mode = findfirst(x->isapprox(imag(x), zero(T)), roots)
    return [roots[!isnothing(unstableMode) ? unstableMode : 1],
            roots[!isnothing(stableMode) ? stableMode : 2],
            roots[!isnothing(marginal_mode) ? marginal_mode : 3]]
end

"""
    marginal_mode(k_perp::NamedTuple,
                  ω::Complex{T},
                  vec::Union{SplineType, Function},
                  plasma::PlasmaParameters
                  problem::LinearProblem,
                 ) where {T, FG <: AbstractFieldlineGeometry}

For a given eigenfunction with unstable eigenvalue `ω` and inteprolated eigenmode `vec`
over the geometry interval `geom`, compute only the marginally stable solution,
which can also provide a check on `compute_linear_roots()`

# See also: [`compute_linear_roots`](@ref)
"""
function marginal_mode(ω::Complex{T},
                       k_perp::NamedTuple,
                       vec::AbstractVector,
                       plasma::PlasmaParameters{T},
                       problem::LinearProblem{3, S, T, E},
                      ) where {S, T, E}
    ky = k_perp.ky

    L3 = L3_integral(vec, problem.cache.B, problem.cache.jac, problem.cache.Bk, plasma)

    L2 = L2_integral(ky, vec, problem.cache.B, problem.cache.jac, problem.cache.Bk, problem.cache.Dk, plasma)

    return -L2 / L3 - 2 * real(ω)
end

function average_k²(k_perp::NamedTuple,
                    vec::AbstractVector,
                    ∂_vec::AbstractVector,
                    problem::LinearProblem{3, S, T, E};
                   ) where {S, T, E}
    k_perp2 = zero(T)
    k_par2 = zero(T)
    norm_val = zero(T)
    @inbounds @simd for i in eachindex(vec, ∂_vec)
        k_perp2 += problem.cache.jac[i] * problem.cache.B[i] * (k_perp.kx ^ 2 * problem.cache.g.xx[i] + 2 * k_perp.kx * k_perp.ky * problem.cache.g.xy[i] + k_perp.ky ^ 2 * problem.cache.g.yy[i]) * abs2(vec[i])
        k_par2 += abs2(∂_vec[i])/(problem.cache.jac[i] * problem.cache.B[i])
        norm_val += problem.cache.jac[i] * problem.cache.B[i] * abs2(vec[i])
    end
    norm_val -= 0.5 * (problem.cache.jac[1] * problem.cache.B[1] * abs2(vec[1]) + problem.cache.jac[end] * problem.cache.B[end] * abs2(vec[end]))
    return ((k_perp2 - 0.5 * (problem.cache.jac[1] * problem.cache.B[1] * (k_perp.kx ^ 2 * problem.cache.g.xx[1] + 2 * k_perp.kx * k_perp.ky * problem.cache.g.xy[1] + k_perp.ky ^ 2 * problem.cache.g.yy[1]) * abs2(vec[1] +
                              problem.cache.jac[end] * problem.cache.B[end] * (k_perp.kx ^ 2 * problem.cache.g.xx[end] + 2 * k_perp.kx * k_perp.ky * problem.cache.g.xy[end] + k_perp.ky ^ 2 * problem.cache.g.yy[end]) * abs2(vec[end])))) / norm_val,
            (k_par2 - 0.5 * (abs2(∂_vec[1]) / (problem.cache.jac[1] * problem.cache.B[1]) +
                             abs2(∂_vec[end]) / (problem.cache.jac[end] * problem.cache.B[end]))) / norm_val)
end

# Simple trapezoid rule for the integrals
function L3_integral(Φ::AbstractVector,
                     B::AbstractVector,
                     sqrtg::AbstractVector,
                     Bk::AbstractVector,
                     plasma::PlasmaParameters{T};
                    ) where {T}
    res = zero(T)
    ρ = 1 + 5 / 3 * plasma.τ
    @inbounds @simd for i in eachindex(Φ, B, sqrtg, Bk)
        res += abs2(Φ[i]) * sqrtg[i] * B[i] * (one(T) + ρ * Bk[i])
    end
    return res - 0.5 * (abs2(Φ[1]) * sqrtg[1] * B[1] * (one(T) + ρ * Bk[1]) +
                        abs2(Φ[end]) * sqrtg[end] * B[end] * (one(T) + ρ * Bk[end]))
end

function L2_integral(ky::T,
                     Φ::AbstractVector,
                     B::AbstractVector,
                     sqrtg::AbstractVector,
                     Bk::AbstractVector,
                     Dk::AbstractVector,
                     plasma::PlasmaParameters{T},
                    ) where {T}
    ρ = 1 + 5 / 3 * plasma.τ
    χ = (plasma.∇T - 2 / 3 * plasma.∇n) * plasma.τ
    @floop SequentialEx() for i in eachindex(Φ, B, sqrtg, Bk, Dk)
        dx = abs2(Φ[i]) * sqrtg[i] * B[i] * (ky * (χ * Bk[i] - plasma.∇n) / B[i] - ρ * Dk[i])
        @reduce(res = zero(T) + dx)
    end
    return res - 0.5 * (abs2(Φ[1]) * sqrtg[1] * B[1] * (ky * (χ * Bk[1] - plasma.∇n) / B[1] - ρ * Dk[1]) +
                        abs2(Φ[end]) * sqrtg[end] * B[end] * (ky * (χ * Bk[end] - plasma.∇n) / B[end] - ρ * Dk[end]))
end

function L1_integral(ky::T,
                     ϕ::AbstractVector,
                     ∂ϕ::AbstractVector,
                     B::AbstractVector,
                     sqrtg::AbstractVector,
                     Dk::AbstractVector,
                     plasma::PlasmaParameters{T},
                    ) where {T}
    @floop SequentialEx() for i in eachindex(∂ϕ, B, sqrtg, Dk)
        dx = -sqrtg[i] * B[i] * (ky * Dk[i] * (plasma.∇T - 2 / 3 * plasma.∇n) / B[i] * plasma.τ) * abs2(ϕ[i]) - (1 + 5 / 3 * plasma.τ) * abs2(∂ϕ[i]) / (sqrtg[i] * B[i])
        @reduce(res = zero(T) + dx)
    end
    return res - 0.5 * (-sqrtg[1] * B[1] * (ky * Dk[1] * (plasma.∇T - 2 / 3 * plasma.∇n) / B[1] * plasma.τ) * abs2(ϕ[1]) - (1 + 5 / 3 * plasma.τ) * abs2(∂ϕ[1]) / (sqrtg[1] * B[1]) + 
                        -sqrtg[end] * B[end] * (ky * Dk[end] * (plasma.∇T - 2 / 3 * plasma.∇n) / B[end] * plasma.τ) * abs2(ϕ[end]) - (1 + 5 / 3 * plasma.τ) * abs2(∂ϕ[end]) / (sqrtg[end] * B[end]))
end

function L0_integral(ky::T,
                     ∂ϕ::AbstractVector,
                     B::AbstractVector,
                     sqrtg::AbstractVector,
                     plasma::PlasmaParameters{T},
                    ) where {T}
    @floop SequentialEx() for i in eachindex(∂ϕ, B, sqrtg)
        dx = -ky * (plasma.∇T - 2 / 3 * plasma.∇n) / B[i] * plasma.τ * abs2(∂ϕ[i]) / (sqrtg[i] * B[i])
        @reduce(res = zero(T) + dx)
    end
    return res - 0.5 * (-ky * (plasma.∇T - 2 / 3 * plasma.∇n) / B[1] * plasma.τ * abs2(∂ϕ[1]) / (sqrtg[1] * B[1]) +
                        -ky * (plasma.∇T - 2 / 3 * plasma.∇n) / B[end] * plasma.τ * abs2(∂ϕ[end]) / (sqrtg[end] * B[end]))
end


#=
"""
    sort_modes(kx,ky,target,evals,evecs,geom,model)

Sorts the modes given by `evals` and `evecs` according to a combintation of growth rate,
k∥² and possibly phase
"""
function sort_modes(dw::Union{DriftWave{3, T, Fields}},
                    geom::FieldlineGeometry{T},
                    problem::LinearProblem{S, T, E};
                    weights::Vector{T}=[0.5,0.25,0.25],
                   ) where {S, T, E, Fields}


  gammas = imag(evals)./maximum(imag(evals))
  kparallel = zeros(T,nev)
  phase_score = zeros(T,nev)
  field_labels = fields(dw)

  for ev_index in eachindex(dw)
    # For the three-field model ϕ will always span the first N points
    ϕ = dw.modes[ev_index][field_labels[1]]
    kparallel[ev_index] = kparallel2(mode.η_span, ϕ, geom.B, geom.jac, problem)
    phase = cross_phase(mode, field_labels[1], field_labels[3], problem.n_points)
    phase_score[ev_index] = sign(phase)*(1.0 - abs(0.5π - abs(phase))/(0.5π))
  end
  kparallel .= 1.0 ./ (kparallel ./ minimum(kparallel))

  mode_score = (weights[1] .* gammas .+  weights[2] .* kparallel
               .+ weights[3] .* phase_score)/sum(weights)
  return sortperm(mode_score)
end
=#

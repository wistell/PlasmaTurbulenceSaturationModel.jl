function update_spectrum!(model::FluidModel{3, T},
                          I::CartesianIndex;
                         ) where {T}
    n_points = model.problem.n_points
    vec = Vector{Complex{T}}(undef, model.problem.n_points)

    if imag(model.problem.eig_vals[1]) > zero(T)

        k_perp = model.grid.k_perp[I]
        index_range = model.index_range[I]
        jac_view = view(model.problem.cache.jac, :)
        B_view = view(model.problem.cache.B, :)
        jac_view[:] .= model.geometry.jac[index_range]
        B_view[:] .= model.geometry.B[index_range]
        for mode_index in 1:n_ev(model)
            β_view = view(model.β, :, mode_index, I) 
            copyto!(vec, model.problem.eig_vecs[1:n_points, mode_index])
            vec ./= maximum(abs, vec)
            β_norm² = zero(T)
            three_wave_norm = zero(Complex{T})
            @inbounds @simd for i in eachindex(β_view)
                β_norm² += jac_view[i] * B_view[i] * abs2(vec[i])
                three_wave_norm += jac_view[i] * B_view[i] * conj(vec[i]) * vec[i] * vec[i]
            end
            β_norm² -= 0.5 * (jac_view[1] * B_view[1] * abs2(vec[1]) + jac_view[end] * B_view[end] * abs2(vec[end]))
            three_wave_norm -= 0.5 * (jac_view[1] * B_view[1] * conj(vec[1]) * vec[1] * vec[1] + jac_view[end] * B_view[end] * conj(vec[end]) * vec[end] * vec[end])

            β_norm² *= step(model.geometry.z_range)
            three_wave_norm *= step(model.geometry.z_range)

            β_view[:] .= vec ./ sqrt(β_norm²)
            model.three_wave_norm[mode_index, I] = three_wave_norm
            
            parallel_derivative!(model.problem.cache.∂_vec, β_view, model.problem)

            ω1 = model.problem.eig_vals[mode_index]     
            ω0 = marginal_mode(ω1, k_perp, β_view, model.plasma, model.problem)
            k_perp², k_par² = average_k²(k_perp, β_view, model.problem.cache.∂_vec, model.problem)

            model.ω[:, mode_index, I] .= [ω1, conj(ω1), ω0]
            coupling_matrix!(model, I, mode_index)
            model.k_perp²[mode_index, I] = k_perp²
            model.k_par²[mode_index, I] = k_par²

        end
        model.computed_solution[I] = true

        if I[1] > 1
            J = CartesianIndex(n_kx_modes(model.grid) - I[1] + 2, I[2])
            
            for mode_index in 1:n_ev(model)

                model.ω[:, mode_index, J] .= model.ω[:, mode_index, I]
                model.β[:, mode_index, J] .= reverse(model.β[:, mode_index, I])
                model.three_wave_norm[mode_index, J] = model.three_wave_norm[mode_index, I]
                coupling_matrix!(model, J, mode_index)
                model.k_perp²[mode_index, J] = model.k_perp²[mode_index, I]
                model.k_par²[mode_index, J] = model.k_par²[mode_index, I]

            end
            model.computed_solution[J] = true
        end
        
    else
        model.computed_solution[I] = true
        model.ω[:, :, I] .= zero(Complex{T})
        model.β[:, :, I] .= zero(Complex{T})
        model.β²[:, :, I] .= zero(T)
        if I[1] > 1
            J = CartesianIndex(n_kx_modes(model.grid) - I[1] + 1, I[2])
            model.computed_solution[J] = true
            model.ω[:, :, J] .= zero(Complex{T})
            model.β[:, :, J] .= zero(Complex{T})
            model.β²[:, :, J] .= zero(T)
        end
    end
    return nothing
end


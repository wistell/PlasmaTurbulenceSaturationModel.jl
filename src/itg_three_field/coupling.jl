
function coupling_matrix!(model::FluidModel{3, T},
                          I::CartesianIndex,
                          mode_index::Int;
                         ) where {T}
    coupling_matrix!(model.coupling_matrix, model.inv_coupling_matrix,
                     I, mode_index, model.ω, model.plasma, model.grid)
    return nothing
end

function coupling_matrix!(coupling_matrix::AbstractArray,
                          inv_coupling_matrix::AbstractArray,
                          I::CartesianIndex,
                          mode_index::Int,
                          ω::AbstractArray,
                          plasma::PlasmaParameters{T},
                          grid::SpectralGrid{T}
                         ) where {T}
    ω = ω[:, mode_index, I]
    ky = grid.k_perp[I].ky
    ηᵢ = plasma.∇T - 2 / 3 * plasma.∇n
    τ = plasma.τ
    m₁₁ = 1.0
    m₂₁ = ky * ηᵢ / ω[1]
    m₂₂ = ky * ηᵢ / ω[2]
    m₂₃ = ky * ηᵢ / ω[3]
    m₃₁ = im / ω[1] * (1 + 5 / 3 * τ + τ * ky * ηᵢ / ω[1])
    m₃₂ = im / ω[2] * (1 + 5 / 3 * τ + τ * ky * ηᵢ / ω[2])
    m₃₃ = im / ω[3] * (1 + 5 / 3 * τ + τ * ky * ηᵢ / ω[3])
    #mass_mat = [m₁₁ m₁₁ m₁₁;
    #            m₂₁ m₂₂ m₂₃;
    #            m₃₁ m₃₂ m₃₃]
    #mass_mat_inv = inv(mass_mat)
    c_mat_view = view(coupling_matrix, :, :, mode_index, I)
    inv_c_mat_view = view(inv_coupling_matrix, :, :, mode_index, I)
    c_mat_view[:] .= [m₁₁, m₂₁, m₃₁, m₁₁, m₂₂, m₃₂, m₁₁, m₂₃, m₃₃]
    inv_c_mat_view[:, :] .= inv(c_mat_view)
    return nothing
end

function compute_couplings!(model::FluidModel{N, T}) where {N, T}
    compute_couplings!(model.τ, model.CC, model.ω, model.coupling_matrix,
                       model.inv_coupling_matrix, model.coupling_index,
                       model.triplet_combos, model.grid)
    return nothing
end

function compute_couplings!(τ::AbstractArray,
                            CC::AbstractArray,
                            ω::AbstractArray, 
                            coupling_matrix::AbstractArray,
                            inv_coupling_matrix::AbstractArray,
                            coupling_index::AbstractArray,
                            triplet_combos::AbstractArray,
                            grid::SpectralGrid{T},
                           ) where {T}

    indices = CartesianIndices(τ)

    @floop ThreadedEx() for i in eachindex(τ, CC)
        local index = indices[i]
        local I = CartesianIndex(index[5], index[6])
        local ev_1 = CartesianIndex(triplet_combos[index[2]][1], I)
        local J = CartesianIndex(index[3], index[4])
        local ev_2 = CartesianIndex(triplet_combos[index[2]][2], J)
        local K = coupling_index[J, I]
        local ev_3 = CartesianIndex(index[1], triplet_combos[index[2]][index[1]], K)
        local kx₁ = grid.k_perp[I].kx
        local kx₂ = grid.k_perp[J].kx
        local ky₁ = grid.k_perp[I].ky
        local ky₂ = grid.k_perp[J].ky
        local ω₁ = conj(ω[1, ev_1])
        local ω₂ = ω[2, ev_2]
        local CC₁ = coupling_matrix[2, 2, ev_2] 
        local CC₂ = coupling_matrix[3, 2, ev_2] 
        local ICC₁ = inv_coupling_matrix[1, 2, ev_1] 
        local ICC₂ = inv_coupling_matrix[1, 3, ev_1] 
        CC[i] = K[2] > 0 ? (kx₁ * ky₂ - ky₁ * kx₂) * (ICC₁ * CC₁ + 0.5 * ICC₂ * CC₂) : (K[2] == 0 && K[1] != 2 * (I[1] - 1) + 1) ? ky₁ * (kx₁ - kx₂) * (ICC₁ * CC₁ + ICC₂ * CC₂) : zero(Complex{T})
        τ[i] = K[2] > 0 ? -im/(ω[ev_3] + ω₂ - ω₁) : (K[2] == 0 && K[1] != 2 * (I[1] - 1) + 1) ? -im/(ω₂ - ω₁) : zero(Complex{T})
    end
    return nothing
end


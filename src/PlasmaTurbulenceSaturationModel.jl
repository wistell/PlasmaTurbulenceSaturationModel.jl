module PlasmaTurbulenceSaturationModel
using Requires
using Statistics
using StaticArrays
using SparseArrays
using LabelledArrays
using LinearAlgebra
using FiniteDifferences
using Polynomials
using LinearMaps
using JacobiDavidson
using KrylovKit
using JLD2
using FFTViews
using FLoops

using PlasmaEquilibriumToolkit

const PTSM = PlasmaTurbulenceSaturationModel
export PTSM

include("types.jl")
include("plasma.jl")
include("linear_problem.jl")
include("geometry.jl")
include("spectral_grid.jl")
#include("drift_wave.jl")
include("fluid_model.jl")
include("utils.jl")
include("linear_solvers.jl")
include("compute_spectrum.jl")
include("coupling.jl")
include("input_output.jl")
include("itg_three_field/itg_three_field.jl")
include("optimization/metrics.jl")
include("visualization.jl")

function __init__()
  # Load the necessary interfaces
    @static if !isdefined(Base, :get_extension)
        @require VMEC="2b46c670-0004-47b5-bf0a-1741584931e9" begin 
            @require GeneTools="34794f4d-a057-4455-8bb4-5e6606ed443e" include("../ext/PlasmaTurbulenceSaturationModelVMECExt.jl")
        end
        @require MPI="da04e1cc-30fd-572f-bb4f-1f8673147195" include("../ext/PlasmaTurbulenceSaturationModelMPIExt.jl")
        @require Makie="ee78f7c6-11fb-53f2-987a-cfe4a2b5a57a" include("../ext/PlasmaTurbulenceSaturationModelMakieExt.jl")
    end

  @require NE3DLE="6356e481-0c20-445f-8811-d253bdede59b" begin
    @require GeneTools="34794f4d-a057-4455-8bb4-5e6606ed443e" include("interfaces/ne3dle_interface.jl")
  end

#=
  @require StellaratorOptimization="f2f44f23-99b2-4d7c-931e-31858dc08d61" begin
    include("optimization/stellopt_interface.jl")
    include("optimization/optimization_targets.jl")
  end

  @require StellaratorOptimization="f2f44f23-99b2-4d7c-931e-31858dc08d61" begin
    @require MPI="da04e1cc-30fd-572f-bb4f-1f8673147195" include("optimization/stellopt_interface_mpi.jl")
  end

  @require CairoMakie="13f3f980-e62b-5c42-98c6-ff1f3baf88f0" begin
    using .CairoMakie
    using LaTeXStrings
    using Printf
    include("visualization/growth_contour.jl")
    include("visualization/mode_spectrum.jl")
    include("visualization/mode_structure.jl")
    include("visualization/tau_contour.jl")
    include("visualization/marg_contour.jl")
  end

  @require GLMakie="e9467ef8-e4e7-5192-8a1a-b1aee30e663a" begin
    using .GLMakie
    using LaTeXStrings
    using Printf
    include("visualization/growth_contour.jl")
    include("visualization/mode_spectrum.jl")
    include("visualization/mode_structure.jl")
    include("visualization/tau_contour.jl")
    include("visualization/marg_contour.jl")
  end
=#
end

end


"""
    FieldlineGeometry <: AbstractFieldlineGeometry

Composite type to hold geometry elements at a single point along a magnetic field line
using GENE geometry normalizations

# Fields
- `z_span::NamedTuple{(:min, :max)}` : not normalized to π
- `g::NamedTuple{(:xx, :xy, :yy)}` : Static vector holding the 6 independent metric elements
- `B` : Magnetic field normalized to magnetic field at LCFS
- `jac` : Scalar Jacobian of the magnetic coordinate transformation
- `Bx∇B::NamedTuple{(:∇x, :∇y)}` : (∇x = B × ∇B ⋅ ∇X / B², ∇y = B × ∇B ⋅ ∇Y / B²)
- `∂B∂z` : ∂B/∂Z, Z = θ in GENE normalizations
"""
struct FieldlineGeometry{T} <: AbstractFieldlineGeometry
    z_range::StepRangeLen
    g::NamedTuple{(:xx, :xy, :yy), Tuple{Vector{T}, Vector{T}, Vector{T}}}
    B::Vector{T}
    jac::Vector{T}
    Bx∇B::NamedTuple{(:∇x, :∇y), Tuple{Vector{T}, Vector{T}}}
    ∂B∂z::Vector{T}
    parameters::SLArray{Tuple{3}, T, 1, 3, (:ι, :s_hat, :nfp)}
end


function FieldlineGeometry(g::AbstractVector{SVector{6, T}},
                           B::AbstractVector{T},
                           J::AbstractVector{T},
                           K1::AbstractVector{T},
                           K2::AbstractVector{T},
                           dBdZ::AbstractVector{T},
                           θ::Union{AbstractVector{T}, AbstractRange};
                           ι::T = 1.0,
                           s_hat::T = 1.0,
                           nfp = 1.0,
                           FloatType = Float64,
                          ) where T
    gxx = map(i -> i[1], g)
    gxy = map(i -> i[2], g)
    gyy = map(i -> i[3], g)
    metric = (xx = gxx, xy = gxy, yy = gyy)
    Bx∇B = (∇x = K1, ∇y = K2)
    θ_range = vector2range(θ)
    return FieldlineGeometry{FloatType}(θ_range, metric, B, J, Bx∇B, dBdZ,
                                        SLVector((ι = ι, s_hat = s_hat, nfp = nfp)))
end

"""
    polarization_damping(z, kx, ky, geom)

Compute the term ``(kx² gˣˣ(z) + 2 kx ky gˣʸ(z) + ky² gʸʸ(z)) / B²(z)``
"""
function polarization_damping(z::S,
                              kx::T,
                              ky::T,
                              geom::FieldlineGeometry;
                             ) where {S, T}
    return polarization_damping(z, kx, ky, 
                                geom.g.xx, geom.g.xy, geom.g.yy, geom.B)
end

function polarization_damping(z::S,
                              kx::T,
                              ky::T,
                              gxx::AbstractVector,
                              gxy::AbstractVector,
                              gyy::AbstractVector,
                              B::AbstractVector;
                             ) where {S, T}
    return (kx^2*gxx[z] + 2*kx*ky*gxy[z] + ky^2*gyy[z])/(B[z]^2)
end

"""
    curvture_drive(z, kx, ky, geom)

Compute the term ``2 (-kx (B × ∇B ⋅ ∇x) + ky * (B × ∇B ⋅ ∇y)) / B`
""" 
function curvature_drive(z::S,
                         kx::T,
                         ky::T,
                         geom::FieldlineGeometry;
                        ) where{S, T}
    return curvature_drive(z, kx, ky, geom.B, geom.Bx∇B.∇x, geom.Bx∇B.∇y)
end

function curvature_drive(z::S,
                         kx::T,
                         ky::T,
                         B::AbstractVector,
                         Bx∇B_∇x::AbstractVector,
                         Bx∇B_∇y::AbstractVector;
                        ) where {S, T}
    # This requires a negative sign in front of K1 as the geometry
    # component given in GENE normalizations is the negative of
    # the proper defintion (I believe due to right-handed vs. left-handed)
    return 2/B[z]*(-kx*Bx∇B_∇x[z] + ky*Bx∇B_∇y[z])
end

function damping_drive_term(z::S,
                            kx::T,
                            ky::T,
                            geom::FieldlineGeometry,
                           ) where {S, T}
    return ((kx^2*geom.g.xx[z] + 2*kx*ky*geom.g.xy[z] + ky^2*geom.g.yy[z])/(geom.B[z]^2),
             2/geom.B[z]*(-kx*geom.Bx∇B.∇x[z] + ky*geom.Bx∇B.∇y[z]))
end

function damping_drive_term!(damp::AbstractVector,
                             drive::AbstractVector,
                             z_index::AbstractVector,
                             kx::T,
                             ky::T,
                             geom::FieldlineGeometry
                            ) where{T}
    for (i, z) in enumerate(z_index)
        damp[i] = polarization_damping(z, kx, ky, geom.g.xx, geom.g.xy, geom.g.yy, geom.B)
        drive[i] = curvature_drive(z, kx, ky, geom.B, geom.Bx∇B.∇x, geom.Bx∇B.∇y)
    end
    return nothing
end

function find_mode_index_range(kx::T,
                               ky::T,
                               geom::FieldlineGeometry,
                               problem::LinearProblem;
                              ) where{T}
    ζₖ = - kx / (geom.parameters.s_hat * ky)
    ζₖ_index = round(Int, ζₖ / step(geom.z_range)) + div(length(geom.z_range), 2) + 1
    return range(start = ζₖ_index - div(problem.n_points, 2), length = problem.n_points)
end

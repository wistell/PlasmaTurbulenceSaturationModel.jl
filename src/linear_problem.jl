struct GeometryCache{T}
    g::NamedTuple{(:xx, :xy, :yy), Tuple{Vector{T}, Vector{T}, Vector{T}}}
    B::Vector{T}
    jac::Vector{T}
    Bk::Vector{T}
    Dk::Vector{T}
    ∂_vec::Vector{Complex{T}}
end

"""
    LinearProblem{N, S <: AbstractSolverMethod, T, E}

Composite type holding the properties for solving the linear drift wave problem
with `N` dynamical fields, `E` dominant + subdominant eiganvalues, floating point
precision `T` with the solver method `S`.
"""
struct LinearProblem{N, S <: AbstractSolverMethod, T, E}
    solver::S
    n_points::Int
    soln_interval::T
    ε::T
    derivative_error_order::Int
    hyper_diffusion_order::Int
    lhs::SparseMatrixCSC{Complex{T}, Int}
    rhs::SparseMatrixCSC{Complex{T}, Int}
    lhs_nrows::Vector{Int}
    operator::SparseMatrixCSC{Complex{T}, Int}
    precond_vec::Vector{Complex{T}}
    target::Vector{Complex{T}}
    buffer::Vector{Complex{T}}
    fdm_stencils::Matrix{Int}
    fdm_coeffs::Matrix{Complex{T}}
    fdm_prefactor::Vector{Complex{T}}
    hyp_diff_stencils::Matrix{Int}
    hyp_diff_coeffs::Matrix{Complex{T}}
    hyp_diff_prefactor::Vector{Complex{T}}
    boundary_coeff_inds::Vector{Vector{CartesianIndex{2}}}
    eig_vals::Vector{Complex{T}}
    eig_vecs::Matrix{Complex{T}}
    cache::GeometryCache{T}
end


"""
    LinearProblem(n_fields; kwargs...)

# Keyword Arguments
- `n_points` : Number of points over which to discretize the field line domain
- `n_ev` : Number of dominant + subdominant eigenvalues to compute at each `(kx, ky)`
- `solver::SolverMethod`=ArnoldiMethod() : Solver method used for linear solver
- `soln_interval::AbstractFloat`=8π : Interval in radians over which to compute linear solutions
- `ε::AbstractFloat`=400.0 : Numerical hyperdiffusion parameter
- `error_order::Integer`=4 : Error order for finite difference discretization
- `hyper_diffusion_order::Integer`=2 : Exponent of hyperdiffusion operator ``(iε)ⁿ∂ⁿ/∂ηⁿ``
- `FloatType` : Floating point precision for the calculation

# See also: (`SolverMethod`)[@ref]
"""
function LinearProblem(n_fields::Int = 3;
                       n_points = 1024,
                       solver=ArnoldiMethod(),
                       soln_interval=8π,
                       n_ev=1,
                       ε=400.0,
                       error_order=4,
                       hyper_diffusion_order=2,
                       FloatType =Float64
                      )

    fdm_stencils, fdm_coeffs, hyp_diff_stencils, hyp_diff_coeffs = setup_finite_diff(error_order, hyper_diffusion_order, FloatType)
    boundary_coeff_inds = setup_boundary_coeff_inds(error_order + 1)

    lhs, rhs, lhs_nrows, operator, buffer = setup_sparse_operators(Val(n_fields), FloatType, n_points,
                                                                   error_order, hyper_diffusion_order, fdm_stencils, hyp_diff_stencils)
    fdm_prefactor = Vector{Complex{FloatType}}(undef, n_points)
    hyp_diff_prefactor = similar(fdm_prefactor)
    eig_vals = Vector{Complex{FloatType}}(undef, n_ev)
    eig_vecs = Matrix{Complex{FloatType}}(undef, n_fields * n_points, n_ev)
    cache = GeometryCache{FloatType}((xx = Vector{FloatType}(undef, n_points), xy = Vector{FloatType}(undef, n_points), yy = Vector{FloatType}(undef, n_points)),
                                     Vector{FloatType}(undef, n_points), Vector{FloatType}(undef, n_points), Vector{FloatType}(undef, n_points),
                                     Vector{FloatType}(undef, n_points), Vector{Complex{FloatType}}(undef, n_points))
    return LinearProblem{n_fields, typeof(solver), FloatType, n_ev}(solver, n_points, soln_interval,
                                                                    ε, error_order, hyper_diffusion_order,
                                                                    lhs, rhs, lhs_nrows, operator,
                                                                    Vector{Complex{FloatType}}(undef, n_fields * n_points),
                                                                    [zero(Complex{FloatType})], buffer,
                                                                    fdm_stencils, fdm_coeffs, fdm_prefactor,
                                                                    hyp_diff_stencils, hyp_diff_coeffs, hyp_diff_prefactor,
                                                                    boundary_coeff_inds, eig_vals, eig_vecs,
                                                                    cache)
end

function n_ev(::LinearProblem{N, S, T, E}) where {N, S, T, E}
    return E
end

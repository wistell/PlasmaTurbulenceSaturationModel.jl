# Set the finite difference grid with the zero boundary condition
"""
    parallel_derivative(rowIndex::Int,maxIndex::int,diffOrder::Int,errorOrder::Int)

Compute the finite difference spline coefficients assuming a zero boundary
condition for the row beyond the first and last row
"""
function parallel_derivative(rowIndex::Int,
                             maxIndex::Int,
                             diffOrder::Int,
                             errorOrder::Int,
                            )
    splineBound = div(errorOrder,2)
    lbound = -splineBound
    ubound = splineBound
    if rowIndex <= splineBound
        lbound = -splineBound + abs(splineBound-rowIndex)
        ubound = errorOrder-rowIndex
    elseif rowIndex > (maxIndex - splineBound)
        lbound = maxIndex - errorOrder - rowIndex + 1
        ubound = maxIndex - rowIndex + 1
    end
    return FiniteDifferenceMethod(lbound:ubound,diffOrder)
end

"""
    setup_finite_diff(error_order::Integer,
                      hyper_diffusion_order::Integer;
                     )


"""
function setup_finite_diff(error_order::Integer,
                           hyper_diffusion_order::Integer,
                           T::Type = Float64;
                          )
    fd_method = parallel_derivative(div(error_order, 2) + 1, error_order + 1, 1, error_order)

    fdm_stencils = zeros(Int, length(fd_method.grid), error_order + 1) 
    fdm_coeffs = zeros(T, length(fd_method.grid), error_order + 1)
    hyp_diff_stencils = zeros(Int, length(fd_method.grid), error_order + 1) 
    hyp_diff_coeffs = zeros(T, length(fd_method.grid), error_order + 1)
    for i in axes(fdm_stencils, 2)
        deriv_op = parallel_derivative(i, error_order + 1, 1, error_order)
        grid_points = deriv_op.grid
        coeffs = deriv_op.coefs
        fdm_stencils[:, i] .= grid_points
        fdm_coeffs[:, i] .= coeffs
      
        hyp_op = parallel_derivative(i, error_order + 1, hyper_diffusion_order, error_order)
        grid_points = hyp_op.grid
        coeffs = hyp_op.coefs
        hyp_diff_stencils[:, i] .= grid_points
        hyp_diff_coeffs[:, i] .= coeffs
    end
    return fdm_stencils, fdm_coeffs, hyp_diff_stencils, hyp_diff_coeffs
end

function setup_boundary_coeff_inds(n_coeffs::Integer)
    n_bdy_rows = div(n_coeffs, 2) + 1
    bdy_offset = div(n_coeffs, 2)
    n_rows = 2 * n_coeffs - 1
    inds = Vector{Vector{CartesianIndex{2}}}(undef, n_rows)
    for index in 1:n_rows
        if index < n_coeffs
            n_fill_rows = index + bdy_offset
            inds[index] = CartesianIndex.(clamp.(reverse(1:n_fill_rows), 1, index + 1), clamp.(1:n_fill_rows, 1, n_bdy_rows))
        elseif index > n_coeffs
            n_fill_rows = n_rows - index + bdy_offset + 1
            inds[index] = CartesianIndex.(clamp.(reverse(n_coeffs-n_fill_rows+1:n_coeffs), index - n_coeffs, n_coeffs),
                                      clamp.(n_coeffs-n_fill_rows+1:n_coeffs, n_bdy_rows, n_coeffs))
        else
            inds[index] = reverse(CartesianIndex.(1:n_coeffs, div(n_coeffs - 1, 2) + 1))
        end
    end
    return inds
end

function parallel_derivative!(res::AbstractVector,
                              vec::AbstractVector,
                              problem::LinearProblem,
                             )
    fill!(res, zero(eltype(vec)))
    error_order = problem.derivative_error_order
    n_points = length(vec)
    n_coeffs = error_order + 1
    n_bdy_rows = div(problem.derivative_error_order, 2)
    stencil_view = @view problem.fdm_stencils[:, n_bdy_rows + 1]
    coeff_view = @view problem.fdm_coeffs[:, n_bdy_rows + 1]
    @inbounds @simd for i in n_bdy_rows+1:n_points-n_bdy_rows
        for j in 1:n_coeffs
            res[i] += coeff_view[j] * vec[i + stencil_view[j]]
        end
    end
    for i in 1:n_bdy_rows
        for j in 2:n_coeffs
            res[i] += problem.fdm_coeffs[j, i] * vec[i + problem.fdm_stencils[j, i]]
        end
        for j in 1:error_order 
            res[n_points - i + 1] += problem.fdm_coeffs[j, n_coeffs - i + 1] * vec[n_points - i + 1 + problem.fdm_stencils[j, n_coeffs - i + 1]]
        end
    end
    return nothing
end

function parallel_derivative(vec::AbstractVector,
                             problem::LinearProblem;
                            )
    res = zeros(eltype(vec), length(vec))
    parallel_derivative!(res, vec, problem)
    return res
end

"""
    vector2range(v)

Convert an evenly spaced vector points to a range
"""
function vector2range(v::AbstractVector{T};
                      convert_type::Type = T,
                     ) where {T}
    if typeof(v) <: AbstractRange
        return v
    else
        v_diff = @view(v[2:end]) .- @view(v[1:end-1])
        v_diff_mean = sum(v_diff)/length(v_diff)
        all(v_diff .≈ v_diff_mean) || throw(error("Step between vector elements must be consistent"))
        return range(convert(T, v[1]), step = convert(T, v_diff_mean), length = length(v))
    end
end

"""
    gaussian_mode(x,σ=1,μ=0.0)

Function for a normalized Gaussian distribution: 1/√(2πσ²)*exp(-(x-μ)²/2σ²)
"""
function gaussian_mode(x, σ=1, μ=0.0)
    return exp(-0.5 * ((x - μ) / σ)^2)/sqrt(2 * π * σ^2)
end

#=
"""
    cross_phase(vec::AbstractVector,nFields::Integer,X::Integer,Y::Integer)

Compute the average cross phase between two different fields given
by contiguous subvectors of `vec` denoted by `X` and `Y`.
For a vector representing the solution to a linear `nFields`-dimensional
system discretized by `M` points, `vec` will have length `M × nFields`
and the field given by `X::Integer` will span `[M × (X-1)+1:M × X]`.
"""
function cross_phase(mode::DriftWave{N, T, Fields},
                     field_1::Union{Int, Symbol},
                     field_2::Union{Int, Symbol},
                     n_points::Int;
                     n_bins::Int = 100,
                    ) where {N, T, Fields}
    ϕ = Vector{Complex{T}}(undef, n_points)
    ψ = similar(ϕ)
    η_range = range(start = mode.η_span.min,
                    stop = mode.η_span.max, length = n_points)
    @inbounds for (i, η) in enumerate(η_range)
        ϕ[i] = getproperty(mode, field_1)(η)
        ψ[i] = getproperty(mode, field_2)(η)
    end

    bins = -π:2π/(n_bins - 1):π
    bin_step = step(bins)
    n_bins2 = div(n_bins, 2)
    weighted_phase_counts = zeros(T, n_bins)

    @inbounds for i = 1:n
        ν = ϕ[i] * conj(ψ[i])
        α = atan(imag(ν), real(ν))
        β = abs(ν)
        
        # Find the binindex
        bindex = floor(Int, α / bin_step) + n_bins2 + 1
        weighted_phase_counts[bindex] += β
    end
    return sum(weighted_phase_counts.*bins)./sum(weighted_phase_counts)
end
=#

"""
    vec_norm(points,vec,geom,problem;quadT=Float64)

Compute the norm of a vector over the field line

# Arguments:
- `index_range::UnitRange` : Range of points
- `vec::Interpolation.Extrapolation` : Interpolated vector of the range of `points`
- `geom::FieldlineGeometry` : Geometry of the field line over the range of `points`
- `problem::LinearProblem` : `LinearProblem` containing the quadrature nodes and weights
"""
function vec_norm(index_range::UnitRange,
                  vec::AbstractVector,
                  geom::FieldlineGeometry{T},
                  problem::LinearProblem{N, S, T, E};
                 ) where {N, S, T, E}
    res = zero(T)
    copyto!(problem.cache.jac, geom.jac[index_range])
    copyto!(problem.cache.B, geom.B[index_range])
    @inbounds @simd for i in eachindex(vec)
        res += problem.cache.jac[i] * problem.cache.B[i] * abs2(vec[i])
    end
    return (res - 0.5 * (problem.cache.jac[1] * problem.cache.B[1] * abs2(vec[1]) +
                         problem.cache.jac[end] * problem.cache.B[end] * abs2(vec[end])))
end

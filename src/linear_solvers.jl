struct direct <: AbstractSolverMethod end

#=
Methods specfic to Jacobi-Davidson
=#
struct JDQZ{T} <: AbstractSolverMethod
    solver::JacobiDavidson.CorrectionSolver
    max_iter::Int
    subspace_dimensions::StepRangeLen
    tolerance::T
end

function JDQZ(rank::Int;
              solver = JacobiDavidson.GMRES,
              correction_iterations = 7,
              max_iter = 10000,
              subspace_dimensions = 50:200,
              tolerance = sqrt(eps()),
             )
    return JQDZ{typeof(tolerance)}(solver(rank, correction_iterations), max_iter, subspace_dimensions, tolerance)
end


struct ArnoldiMethod{T} <: AbstractSolverMethod
    howmany::Int
    krylovdim::Int 
    maxiter::Int
    tol::T
end

"""
    ArnoldiMethod(; howmany = 25, krylovdim = 50, maxiter = 1000, tolerance = 1e-6)

Initialize an `ArnoldiMethod` struct for computing `howmany` eigenmodes per point
with a shift-and-invert iterative method
"""
function ArnoldiMethod(;
                 howmany = 25,
                 krylovdim = 50,
                 maxiter = 1000,
                 tolerance = 1e-6,
                )
    return ArnoldiMethod{typeof(tolerance)}(howmany, krylovdim, maxiter, tolerance)
end


function compute_eigenmodes!(I::CartesianIndex,
                             model::FluidModel{N, T};
                             kwargs...
                            ) where{N, T}
    #lhs, rhs = sparse.(dense_linear_operators(model.spectrum[I], model.geometry,
    #                                          model.problem, model.plasma))
    build_linear_operators!(model.problem, model.grid.k_perp[I], model.index_range[I], model.geometry, model.plasma)
    compute_eigenmodes!(model.problem; kwargs...)
    return nothing
end


function compute_eigenmodes(problem::LinearProblem{N, S, T, E};
                            target::Complex{T} = one(T) * im,
                            kwargs...
                           ) where {N, S <: JDQZ, T, E}
    pschur, _ = jdqz(problem.lhs, problem.rhs,
                     solver= problem.solver.solver,
                     target= Near(real(target)+10im*imag(target)),
                     pairs= E,
                     subspace_dimensions = problem.solver.subspace_dimensions,
                     max_iter = problem.solver.max_iter,
                     tolerance = problem.solver.tolerance,
                     numT = Complex{T},
                     verbosity=1);

    Q = pschur.Q.basis
    Z = pschur.Z.basis
    evals, evecs = eigen(Z'*lhs*Q, Z'*rhs*Q)
    return evals, Q*evecs
end

function compute_eigenmodes!(problem::LinearProblem{N, S, T, E},
                             kwargs...
                            ) where {N, S <: ArnoldiMethod, T, E}
    rank = size(problem.lhs, 1)
    σ = problem.target[]
    β = !iszero(σ) ? ComplexF64.(problem.precond_vec) : problem.precond_vec

    op = if !iszero(σ) 
        lu_op = lu(problem.operator - σ * I)
        LinearMap{Complex{T}}((y, x) -> ldiv!(y, lu_op, x), rank, ismutating = true)
    else
        problem.operator
    end        

    sort_mode = !iszero(σ) ? :LM : :LI
    shift_evals, evecs, _  = KrylovKit.eigsolve(op, β, problem.solver.howmany, sort_mode;
                                                tol = problem.solver.tol,
                                                krylovdim = problem.solver.krylovdim,
                                                maxiter = problem.solver.maxiter)
    op = nothing

    evals = !iszero(σ) ? σ .+ inv.(shift_evals) : shift_evals
    sort_order = sortperm(imag(evals), rev = true)
    for i in 1:E
        problem.eig_vecs[:, i] .= evecs[sort_order[i]][:]
    end
    copyto!(problem.eig_vals, evals[sort_order[1:E]])
    #trunc_digits = -Int(round(log10(problem.solver.tol)))
    #map!(e -> trunc(real(e), digits = trunc_digits) + trunc(imag(e), digits = trunc_digits)*im, problem.eig_vals, evals[sort_order[1:E]])
    return nothing
end




struct DriftWave{N, T, Fields}
    I::CartesianIndex
    k_perp::SpectralPoint{T}
    parameters::LArray{T, 1, Vector{T}, (:amplitude, :norm)}
    η_span::BoundsTuple{T}
    index_range::UnitRange
    ω::Vector{Complex{T}}
    β::Vector{Complex{T}}
    k²::LArray{T, 1, Vector{T}, (:perp, :parallel)}
    coupling_matrix::Array{Complex{T}, 3}
    inv_coupling_matrix::Array{Complex{T}, 3}
end

function DriftWave(n_points::Int,
                   field_labels::NTuple{N, Symbol};
                  ) where {N}
    return DriftWave((0, 0), 0.0, 0.0, 1.0, -1.0π, 1.0π, 1:n_points, field_labels)
end

function DriftWave()
    return DriftWave(1024, (:Φ, :U, :T))
end

function DriftWave(I, kx, ky, amp, η_span, ind_range, ω, β, norm, k_perp2, k_parallel2, c_mat, inv_c_mat, field_labels)
    #size(ω) == size(β) || throw(DimensionMismatch("Incompatible arrays of eigenvalues and eigenvectors"))
    size(c_mat, 1) == size(c_mat, 2) || throw(DimensionMismatch("Coupling matrix must be square"))
    size(c_mat, 1) == size(ω, 1) || throw(DimensionMismatch("Incompatible ranks between eigenvalues and coupling matrix"))
    N = size(ω, 1)
    T = eltype(real(ω))
    field_labels = field_labels
    if !(field_labels isa NTuple{N, Symbol})
        @warn "Incompatible number of field labels, setting field labels to (:a, :b, ...)"
        field_labels = (Symbol.(collect(range(start = 'a', step = 1, length = N))...))
    end
    return DriftWave{N, T, field_labels}(CartesianIndex(I),
                                         SpectralPoint{T}(kx, ky),
                                         LVector(amplitude = amp, norm = norm),
                                         η_span, ind_range, ω, β,
                                         LVector(perp = k_perp2, parallel = k_parallel2),
                                         c_mat, inv_c_mat)
end

function DriftWave(I, kx, ky, amp, η_min, η_max, ind_range, field_labels; FloatType = Float64)
    N = length(field_labels)
    ω = zeros(Complex{FloatType}, N)
    η_span = (min = η_min, max = η_max)
    β = Vector{Complex{FloatType}}(undef, N * length(ind_range))
    norms = one(FloatType)
    k_perp2 = zero(FloatType)
    k_parallel2 = zero(FloatType)
    c_mat = Array{Complex{FloatType}}(undef, N, N, length(ind_range))
    inv_c_mat = similar(c_mat)
    return DriftWave(I, kx, ky, amp, η_span, ind_range, ω, β, norms, k_perp2, k_parallel2, c_mat, inv_c_mat, field_labels)
end

# Extend the get index function to return
# the eigenvalue
function Base.getindex(u::DriftWave{N, T, Fields},
                        i::Int;
                       ) where {N, T, Fields}
    return getfield(u, :ω)[i]
end

function Base.getindex(u::DriftWave{N, T, Fields},
                       s::Symbol;
                      ) where {N, T, Fields}
    return getproperty(u, s)
end

function Base.length(u::DriftWave{N, T, Fields}) where {N, T, Fields}
    return length(u.index_range)
end

# Extend the getproperty function to return
# the eigenvalue field_labels as well
function Base.getproperty(u::DriftWave{N, T, Fields},
                          s::Symbol;
                        ) where {N, T, Fields}
    ind = findfirst(f -> f === s, Fields)
    return !isnothing(ind) ? view(getfield(u, :β), ((ind - 1)*length(u) + 1):(ind * length(u))) : getfield(u, s)
end

function n_fields(::DriftWave{N, T, Fields}) where {N, T, Fields}
    return N
end

function float_type(::DriftWave{N, T, Fields}) where {N, T, Fields}
    return T
end

function fields(::DriftWave{N, T, Fields}) where {N, T, Fields}
    return Fields
end

mutable struct DriftWaveBundle{N, T, E, Fields}
    k_perp::SpectralPoint{T}
    η_span::BoundsTuple{T}
    index_range::UnitRange
    modes::Vector{DriftWave{N, T, Fields}}
    computed_solution::Vector{Bool}

    DriftWaveBundle{N, T, E, Fields}(a, b, c, d, e) where {N, T, E, Fields} = new{N, T, E, Fields}(a, b, c, d, e)
    DriftWaveBundle{N, T, E, Fields}(n_points) where {N, T, E, Fields} = new{N, T, E, Fields}(SpectralPoint{T}(kx, ky),
                                                                                              (min = -1.0π, max = 1.0π),
                                                                                              1:n_points,
                                                                                              fill(DriftWave((0, 0), kx ,ky, one(T), -1.0π, 1.0π, 1:n_points, Fields), N),
                                                                                              [false])
    DriftWaveBundle() = DriftWaveBundle{3, Float64, 1, (:Φ, :U, :T)}(1024)
end

function DriftWaveBundle(modes::Vector{DriftWave{N, T, Fields}};
                         computed_solution = true,
                        ) where {N, T, Fields} 
    E = length(modes)
    k_perp = first(modes).k_perp
    η_span = first(modes).η_span
    index_range = first(modes).index_range
    return DriftWaveBundle{N, T, E, Fields}(k_perp, η_span, index_range, modes, [computed_solution])
end


function DriftWaveBundle(modes::Vector{DriftWave{N, T, Fields}},
                         η_span::BoundsTuple,
                         index_range::UnitRange;
                         computed_solution = true,
                        ) where {N, T, Fields} 
    E = length(modes)
    k_perp = first(modes).k_perp
    return DriftWaveBundle{N, T, E, Fields}(k_perp, η_span, index_range, modes, [computed_solution])
end

function DriftWaveBundle(I::Union{CartesianIndex, NTuple{2, Int}},
                         kx::T,
                         ky::T,
                         amp::T,
                         η_min = -1.0π,
                         η_max = 1.0π,
                         index_range::UnitRange = 1:128; 
                         n_fields = 3, 
                         n_ev = 1, 
                         FloatType = Float64, 
                         field_labels = (:Φ, :U, :T),
                         computed_solution::Bool = false,
                        ) where {T}
    length(field_labels) == n_fields || throw(error("Number of field labels must match the number of field_labels"))
    modes = [DriftWave(CartesianIndex(I), kx, ky, amp, η_min, η_max, index_range, field_labels; FloatType = FloatType) for _ in 1:n_ev]
    return DriftWaveBundle(modes; computed_solution = computed_solution)
end

function Base.getindex(b::DriftWaveBundle{N, T, E, Fields},
                       i::Int;
                      ) where {N, T, E, Fields}
    return view(b.modes, i)[]
end

function Base.eachindex(b::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return eachindex(b.modes)
end

function fields(::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return Fields
end

function n_ev(::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return E
end

function float_type(::DriftWaveBundle{N, T, E, Fields}) where {N, T, E, Fields}
    return T
end

"""
    PlasmaParameters

Composite type holding the plasma parameters for a fluid model calculations,
current fields are `∇T`, `∇n`, `τ` and `β`

"""
struct PlasmaParameters{T}
    # Normalized temperature gradient: ∇T = -L_ref/T dT/dr"
    ∇T::T

    # Normalized density gradient: ∇n = -L_ref/n dn/dr
    ∇n::T

    # Ion-to-electron temperature ration
    τ::T

    # Normalized ratio of plasma pressure to magnetic pressure
    β::T
end

"""
    PlasmaParameters(∇T::Real,∇n::Real,τ::Real=0,β::Real=0)

Constructor for `PlasmaParameters` type taking real arguments
"""
function PlasmaParameters(∇T,
                          ∇n,
                          τ=1.0,
                          β=0.0;
                          FloatType::Type = Float64,
                         )
  ∇T_2, ∇n_2, τ_2, β_2 = convert.(FloatType, promote(∇T, ∇n, τ, β))
  return PlasmaParameters{typeof(∇T_2)}(∇T_2, ∇n_2, τ_2, β_2)
end


